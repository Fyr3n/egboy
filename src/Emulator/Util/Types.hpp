//
// Created by Fyren on 29/09/2018.
//

#ifndef GAMEBOYCOLORPROJECT_TYPES_HPP
#define GAMEBOYCOLORPROJECT_TYPES_HPP

#include <cstdint>

typedef uint8_t   u8;   typedef int8_t  s8;
typedef uint16_t u16;   typedef int16_t s16;
typedef uint32_t u32;   typedef int32_t s32;

#endif //GAMEBOYCOLORPROJECT_TYPES_HPP
